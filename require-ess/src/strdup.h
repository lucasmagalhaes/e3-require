/* Copyright (C) 2020 Dirk Zimoch */
/* Copyright (C) 2020-2022 European Spallation Source, ERIC */

#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

char *strdup(const char *s);
char *strndup(const char *s, size_t n);

#ifdef __cplusplus
}
#endif  // __cplusplus
