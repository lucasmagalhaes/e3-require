#!/usr/bin/env bash
#
#  Copyright (c) 2017 - 2019     Jeong Han Lee
#  Copyright (c) 2017 - 2019     European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#   Shell   : setE3Env.bash
#   Author  : Jeong Han Lee
#   email   : jeonghan.lee@gmail.com
#   date    : Friday, November  8 22:48:29 CET 2019
#   version : 0.8.1
#
#   author  : Wayne Lewis
#   email   : waynelewis@ess.eu
#   date    : Wed Jul  7 19:46:38 CEST 2021
#

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
  echo
  echo "##################################################"
  echo "$0 is NOT properly sourced !"
  echo "##################################################"
  echo
  exit 1
fi

# the following function drop_from_path was copied from
# the ROOT build system in ${ROOTSYS}/bin/, and modified
# a little to return its result
# Wednesday, July 11 23:19:00 CEST 2018, jhlee

drop_from_path() {
  #
  # Assert that we got enough arguments
  if test $# -ne 2; then
    echo "drop_from_path: needs 2 arguments"
    return 1
  fi

  local p="$1"
  local drop="$2"

  echo "$p" | sed -e "s;:${drop}:;:;g" \
    -e "s;:${drop};;g" \
    -e "s;${drop}:;;g" \
    -e "s;${drop};;g"
}

set_variable() {
  if test $# -ne 2; then
    echo "set_variable: needs 2 arguments"
    return 1
  fi

  local old_path="$1"
  local add_path="$2"

  local new_path=""
  local system_old_path=""

  if [ -z "$old_path" ]; then
    new_path=${add_path}
  else
    system_old_path=$(drop_from_path "${old_path}" "${add_path}")
    if [ -z "$system_old_path" ]; then
      new_path=${add_path}
    else
      new_path=${add_path}:${system_old_path}
    fi

  fi

  echo "${new_path}"

}

print_env() {

  local disabled="$1"
  shift

  if [ "$disabled" = "no_msg" ]; then
    printf "\n"
  else
    printf "\nSet the ESS EPICS Environment as follows:\n"
    printf "THIS Source NAME    : %s\n" "${SRC_NAME}"
    printf "THIS Source PATH    : %s\n" "${SRC_PATH}"
    printf "EPICS_BASE          : %s\n" "${EPICS_BASE}"
    printf "EPICS_HOST_ARCH     : %s\n" "${EPICS_HOST_ARCH}"
    printf "E3_REQUIRE_LOCATION : %s\n" "${E3_REQUIRE_LOCATION}"
    printf "PATH                : %s\n" "${PATH}"
    printf "LD_LIBRARY_PATH     : %s\n" "${LD_LIBRARY_PATH}"
    printf "\n"
    printf "Enjoy E3!\n"
  fi
}

# Reset all EPICS and E3 related pre-existing variables
# Remove them from PATH and LD_LIBRARY_PATH
#
# If EPICS_BASE is defined,
# 1) Remove EPICS_BASE bin in the system PATH
# 2) Remove EPICS_BASE lib in the system LD_LIBRARY_PATH
# 3) Unset EPICS_BASE, EPICS_HOST_ARCH, and so on
if [ -n "$EPICS_BASE" ]; then

  system_path=${PATH}
  drop_base_path="${EPICS_BASE}/bin/${EPICS_HOST_ARCH}"

  PATH=$(drop_from_path "${system_path}" "${drop_base_path}")
  export PATH

  system_ld_path=${LD_LIBRARY_PATH}
  drop_ld_path="${EPICS_BASE}/lib/${EPICS_HOST_ARCH}"

  LD_LIBRARY_PATH=$(drop_from_path "${system_ld_path}" "${drop_ld_path}")
  export LD_LIBRARY_PATH

  # If E3_REQUIRE_NAME exists, it is E3
  if [ -n "$E3_REQUIRE_NAME" ]; then

    e3_path=${PATH}

    PATH=$(drop_from_path "${e3_path}" "${E3_REQUIRE_BIN}")
    export PATH

    # Drop the require-related LD_LIBRARY paths set previously
    LD_LIBRARY_PATH=$(drop_from_path "$LD_LIBRARY_PATH" "${E3_REQUIRE_LOCATION}[^:]*")
    export LD_LIBRARY_PATH

    unset E3_REQUIRE_NAME
    unset E3_REQUIRE_VERSION
    unset E3_REQUIRE_LOCATION

    unset E3_REQUIRE_BIN
    unset E3_REQUIRE_LIB
    unset E3_REQUIRE_INC
    unset E3_REQUIRE_DB

    unset E3_SITEMODS_PATH

    unset EPICS_DRIVER_PATH

    unset SCRIPT_DIR

  fi

  unset EPICS_BASE
  unset EPICS_HOST_ARCH

fi

THIS_SRC=${BASH_SOURCE[0]:-${0}}

if [ -L "$THIS_SRC" ]; then
  SRC_PATH="$(cd -P "$(dirname "$(readlink -f "$THIS_SRC")")" && pwd)"
else
  SRC_PATH="$(cd -P "$(dirname "$THIS_SRC")" && pwd)"
fi

SRC_NAME=${THIS_SRC##*/}

E3_REQUIRE_LOCATION=${SRC_PATH%/*}
E3_REQUIRE_VERSION=${E3_REQUIRE_LOCATION##*/}
E3_REQUIRE_PATH=${E3_REQUIRE_LOCATION%/*}
E3_REQUIRE_NAME=${E3_REQUIRE_PATH##*/}
EPICS_BASE=${E3_REQUIRE_PATH%/*}

EPICS_HOST_ARCH=$(perl "${EPICS_BASE}"/lib/perl/EpicsHostArch.pl)

E3_REQUIRE_BIN=${E3_REQUIRE_LOCATION}/bin
E3_REQUIRE_LIB=${E3_REQUIRE_LOCATION}/lib
E3_REQUIRE_INC=${E3_REQUIRE_LOCATION}/include
E3_REQUIRE_DB=${E3_REQUIRE_LOCATION}/db
E3_REQUIRE_DBD=${E3_REQUIRE_LOCATION}/dbd

E3_SITEMODS_PATH=${E3_REQUIRE_LOCATION}/siteMods

EPICS_DRIVER_PATH=${E3_SITEMODS_PATH}

export EPICS_BASE
export E3_REQUIRE_NAME
export E3_REQUIRE_VERSION

export EPICS_HOST_ARCH
export E3_REQUIRE_LOCATION

export E3_REQUIRE_BIN
export E3_REQUIRE_LIB
export E3_REQUIRE_INC
export E3_REQUIRE_DB
export E3_REQUIRE_DBD

export E3_SITEMODS_PATH

export EPICS_DRIVER_PATH

old_path=${PATH}
E3_PATH="${E3_REQUIRE_BIN}:${EPICS_BASE}/bin/${EPICS_HOST_ARCH}"

PATH=$(set_variable "${old_path}" "${E3_PATH}")

export PATH

old_ld_path=${LD_LIBRARY_PATH}
E3_LD_LIBRARY_PATH="${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${E3_REQUIRE_LIB}/${EPICS_HOST_ARCH}"
LD_LIBRARY_PATH=$(set_variable "${old_ld_path}" "${E3_LD_LIBRARY_PATH}")

export LD_LIBRARY_PATH

# Add iocsh autocompletion
# shellcheck source=require-ess/tools/iocsh_complete.bash
source "${SRC_PATH}"/iocsh_complete.bash

print_env "$1"

# Set the bash prompt to show information about the e3 environment
BASE_version="${EPICS_BASE##*/base-}"
E3_ENV_INFO="(${BASE_version}-${E3_REQUIRE_VERSION})"
NEW_E3_PS="${E3_ENV_INFO}\[\033[33m\]\u\[\033[m\]@\[\033[33;1m\]\w\[\033[m\]\$ "

if [ -z "$E3_ENV_ON" ]; then
  if [ -z "$PS1" ]; then
    export E3_ENV_ON="ON"
    export PS1="${NEW_E3_PS}"
  else
    export E3_ENV_ON="ON"
    export PS1="${E3_ENV_INFO}${PS1}"
  fi
else
  PS_TMP=${PS1#*\)}
  export PS1="${E3_ENV_INFO}${PS_TMP}"
fi
