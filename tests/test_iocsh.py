import re

from .utils import Wrapper, run_ioc_get_output


def test_multiple_cell_paths(wrapper: Wrapper):
    cell_path_a = wrapper.path / "cellModsA"
    wrapper.run_make("cellinstall", cell_path=cell_path_a)

    cell_path_b = wrapper.path / "cellModsB"
    wrapper.run_make("cellinstall", cell_path=cell_path_b)

    rc, outs, _ = run_ioc_get_output(
        "-l", cell_path_a, "-l", cell_path_b, module=wrapper.name
    )
    assert rc == 0

    regex = r"Module {name} version {version} found in {path}"
    assert not re.search(
        regex.format(
            name=re.escape(wrapper.name),
            version=re.escape(wrapper.version),
            path=re.escape(str(cell_path_a)),
        ),
        outs,
    )

    assert re.search(
        regex.format(
            name=re.escape(wrapper.name),
            version=re.escape(wrapper.version),
            path=re.escape(str(cell_path_b)),
        ),
        outs,
    )


def test_cellpath_does_not_depend_on_order(wrapper: Wrapper):
    cell_path = wrapper.path / "cellMods"

    wrapper.run_make("cellinstall", cell_path=cell_path)

    rc, *_ = run_ioc_get_output("-l", cell_path, "-r", wrapper.name)
    assert rc == 0

    rc, *_ = run_ioc_get_output("-r", wrapper.name, "-l", cell_path)
    assert rc == 0
