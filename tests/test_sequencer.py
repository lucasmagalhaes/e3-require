import pathlib

import pytest

from .utils import Wrapper

GITLAB_URL = "https://gitlab.esss.lu.se"

TEST_SEQ_SRC = """
program test

ss ss1 {{
    state init {{
        when(delay(1)) {{
            printf({});
        }}
        state init
    }}
}}
"""


class Sequencer(Wrapper):
    sequencer_url = f"{GITLAB_URL}/e3/wrappers/core/e3-sequencer.git"

    def __init__(self, path: pathlib.Path):
        super().__init__(path, name="sequencer", url=self.sequencer_url)

        self.version = "sequencer_test"

        self.write_dot_local_data(
            "RELEASE",
            {"EPICS_BASE": self.epics_base, "E3_REQUIRE_VERSION": self.require_version},
        )
        self.write_dot_local_data("CONFIG_MODULE", {"E3_MODULE_VERSION": self.version})

        self.cell_path = self.path / "cellMods"

        rc, *_ = self.run_make("init")
        assert rc == 0
        rc, *_ = self.run_make("patch")
        assert rc == 0
        rc, *_ = self.run_make("build")
        assert rc == 0
        rc, *_ = self.run_make("cellinstall", cell_path=self.cell_path)
        assert rc == 0

        self.snc_path = (
            self.cell_path
            / f"base-{self.base_version}"
            / f"require-{self.require_version}"
            / "sequencer"
            / self.version
            / "bin"
            / self.host_arch
            / "snc"
        )
        assert self.snc_path.is_file()


@pytest.fixture(scope="module")
def sequencer(tmp_path_factory):
    path = tmp_path_factory.mktemp("sequencer_build")
    yield Sequencer(path)


class SNLWrapper(Wrapper):
    def __init__(self, sequencer: Sequencer, path: pathlib.Path):
        super().__init__(path)
        self.sequencer = sequencer

        self.add_var_to_config_module("SEQUENCER_DEP_VERSION", self.sequencer.version)

    def run_make(self, *args):
        return super().run_make(
            *args,
            f"E3_CELL_PATH={self.sequencer.cell_path}",
            f"SNC={self.sequencer.snc_path}",
        )


@pytest.fixture
def snl_wrapper(sequencer, tmp_path):
    yield SNLWrapper(sequencer, tmp_path)


@pytest.mark.parametrize("extension", ["st", "stt"])
def test_compile_snl_file(snl_wrapper: SNLWrapper, extension):
    snl_filename = "test_filename"
    seq_source = snl_wrapper.module_dir / f"{snl_filename}.{extension}"
    seq_source.write_text(TEST_SEQ_SRC.format('""'))

    snl_wrapper.add_var_to_module_makefile("SOURCES", f"{snl_filename}.{extension}")

    rc, *_ = snl_wrapper.run_make("cellbuild")
    assert rc == 0

    assert (snl_wrapper.build_dir / f"{snl_filename}.o").is_file()
    assert (snl_wrapper.build_dir / f"{snl_filename}_snl.dbd").is_file()


def test_preprocess_st_file(snl_wrapper: SNLWrapper):
    snl_filename = "test_file.st"
    seq_src = snl_wrapper.module_dir / snl_filename
    seq_src.write_text(
        '#define MESSAGE "waiting\\n"\n' + TEST_SEQ_SRC.format("MESSAGE")
    )

    snl_wrapper.add_var_to_module_makefile("SOURCES", snl_filename)

    rc, *_ = snl_wrapper.run_make("cellbuild")
    assert rc == 0


def test_do_not_preprocess_stt_file(snl_wrapper: SNLWrapper):
    snl_filename = "test_file"
    seq_src = snl_wrapper.module_dir / f"{snl_filename}.stt"
    seq_src.write_text(
        '#define MESSAGE "waiting\\n"\n' + TEST_SEQ_SRC.format("MESSAGE")
    )

    snl_wrapper.add_var_to_module_makefile("SOURCES", f"{snl_filename}.stt")

    rc, _, errs = snl_wrapper.run_make("cellbuild")
    assert rc == 2
    assert (
        f"No rule to make target `{snl_filename}.c', needed by `{snl_filename}_snl.dbd'"
        in errs
    )
    assert not (snl_wrapper.build_dir / f"{snl_filename}.i").is_file()
