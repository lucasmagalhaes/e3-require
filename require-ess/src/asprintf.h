/* Copyright (C) 2020 Dirk Zimoch */
/* Copyright (C) 2020-2022 European Spallation Source, ERIC */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

#include <stdarg.h>

#ifndef __GNUC__
#define __attribute__(arg)
#endif  // __GNUC__

int asprintf(char **pbuffer, const char *format, ...)
    __attribute__((__format__(__printf__, 2, 3)));
int vasprintf(char **pbuffer, const char *format, va_list ap)
    __attribute__((__format__(__printf__, 2, 0)));

#ifdef __cplusplus
}
#endif  // __cplusplus
