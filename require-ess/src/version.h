/* Copyright (C) 2020-2022 European Spallation Source, ERIC */

#pragma once

typedef struct semver_t {
  char *version_str;
  int major;
  int minor;
  int patch;
  int build;  // can be negative; implies that build has not been specified
  char *test_str;
} semver_t;

void cleanup_semver(semver_t *s);

int parse_semver(const char *version, semver_t *s);
