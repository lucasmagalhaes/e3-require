import subprocess

from .utils import Wrapper


def test_can_run_make_in_wrapper_directory(wrapper: Wrapper):
    results = subprocess.run(["make", "-C", wrapper.path, "vars"])
    assert results.returncode == 0
