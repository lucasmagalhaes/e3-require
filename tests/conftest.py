from pathlib import Path

import pytest

from .utils import Wrapper


@pytest.fixture
def wrappers(tmpdir):
    class WrapperFactory:
        def get(self, **kwargs):
            return Wrapper(Path(tmpdir), **kwargs)

    yield WrapperFactory()


@pytest.fixture
def wrapper(wrappers):
    yield wrappers.get()
