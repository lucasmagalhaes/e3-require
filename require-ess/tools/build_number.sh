#!/usr/bin/env bash
# Small script to fetch current build nuber for a given module. If you pass a numeric version
# but no build number, it fetches the latest build number installed from the target e3 env.
#
# Arguments:
#    $1: require module path e.g. /epics/base-$BASE_VERSION/require/$REQUIRE_VERSION/siteMods
#    $2: module name
#    $3: module version
shopt -s extglob

[[ $# -ne 3 ]] && exit 1

p="$1"
m="$2"
v="$3"

if [[ $v =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  # shellcheck disable=SC2012
  basename "$(ls -dvr "$p/$m/$v"?(++([0-9])) 2>/dev/null | head -n 1)"
else
  echo "$v"
fi
