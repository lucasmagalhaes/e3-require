from .utils import Wrapper


def test_loc(wrappers):
    wrapper = wrappers.get(E3_MODULE_SRC_PATH="test-loc")
    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert 'Local source mode "-loc" has been deprecated' in errs


def test_sitelibs(wrapper: Wrapper):
    makefile_contents = wrapper.makefile.read_text()

    wrapper.makefile.write_text(
        makefile_contents.replace(
            "include $(REQUIRE_CONFIG)/RULES_SITEMODS",
            "include $(REQUIRE_CONFIG)/RULES_E3",
        )
    )

    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert "RULES_E3 should only be loaded from RULES_SITEMODS" in errs


def test_incorrect_module_name(wrappers):
    module_name = "ADCore"

    wrapper = wrappers.get(name=module_name)
    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert f"E3_MODULE_NAME '{module_name}' is not valid" in errs


def test_add_missing_build_number(wrapper: Wrapper):
    version = "1.2.3"
    wrapper.write_dot_local_data("CONFIG_MODULE", {"E3_MODULE_VERSION": version})

    rc, outs, _ = wrapper.run_make("vars")
    assert rc == 0
    assert f"E3_MODULE_VERSION = {version}+0" in outs.splitlines()


def test_warning_if_building_from_sitemods(wrapper: Wrapper):
    rc, _, errs = wrapper.run_make(
        "build_path_check", E3_MODULES_PATH=wrapper.path / ".."
    )
    assert rc == 0
    assert (
        "It is ill-advised to build a module from the module install location." in errs
    )
